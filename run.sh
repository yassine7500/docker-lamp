#! /bin/bash


DIR=$(cd `dirname $0` && pwd)

#Check if fauria/lamp is installed
if docker images fauria/lamp | grep -q 'fauria';then
        echo "fauria lamp installed";
    else
        echo 'pulling fauria';
        docker pull fauria/lamp
fi


#check if the base directories exist and create them if not
if [ ! -d "$DIR/www" ];then
    mkdir "$DIR/www"
fi
if [ ! -d "$DIR/log" ];then
    mkdir "$DIR/log"
    mkdir "$DIR/log/mysql"
    mkdir "$DIR/log/httpd"
fi
if [ ! -d "$DIR/www" ];then
    mkdir "$DIR/lib"
    mkdir "$DIR/lib/mysql"
fi

if [ ! -d "$DIR/config" ];then
    mkdir "$DIR/config"
fi


#configuration files
APP_DIR="$DIR/www";
APACHE_CONFIG_DIR="$DIR/config/apache2/sites-enabled";
LOG_HTTPD="$DIR/log/httpd";
LOG_MYSQL="$DIR/log/mysql";
LIB_MYSQL="$DIR/lib/mysql";

echo "Insert port to map to the docker container (Press Enter for default port 80)"
read PORT

if [ -z ${var+x} ]; then
    PORT=80
fi

docker run -d -p 80:$PORT -v $APP_DIR:/var/www/html -v $APACHE_CONFIG_DIR:/etc/apache2/sites-enabled fauria/lamp 

echo "Docker is running on port $PORT"
